"use client";
import React from "react";
import Image from "next/image";
import Link from "next/link";
import { ChevronDown, MapPin, Search, LocateFixed, LogIn, ArrowRight } from "lucide-react";
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card";
import Imageslider from "@/components/Imageslider";

export default function Comingsoon() {
  return (
    <div className="container mx-auto max-2xl px-0 lg:max-w-[1920px] text-center lg:text-left py-2 lg:py-8 lg:h-screen">
      <div className="px-4 h-full">
        <div className="flex -mx-4 h-full flex-wrap">
          <div className="px-4 w-full lg:w-2/4 h-full pb-7 lg:pb-0">
            <div className="flex items-center justify-center h-full">
              <div className="px-5 md:px-10">
                <div className="flex items-center justify-center pt-4 lg:pt-10 pb-5">
                  <Link href="/">
                    <Image alt="FIX IT TODAY" src="../images/guide-logo.svg" width="200" height="35"></Image>
                  </Link>
                </div>

                <div className="flex flex-col items-center justify-center pt-8 2xl:pt-16 text-center">
                  <h1 className="text-3xl md:text-4xl xl:text-6xl text-blue-500 animate-bounce drop-shadow-2xl uppercase font-bold">Coming Soon</h1>

                  <p className="text-black text-sm opacity-70 pt-6 max-w-md mx-auto">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&lsquo;s standard dummy since
                    the 1500s.
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="px-4 w-full lg:w-2/4 flex items-center overflow-hidden relative  bg-white before:absolute before:left-0 before:top-0 before:z-[2] before:h-full before:w-[100px] before:bg-[linear-gradient(to_right,white_0%,rgba(255,255,255,0)_100%)] before:content-[''] after:absolute after:right-0 after:top-0 after:z-[2] after:h-full after:w-[100px] after:-scale-x-100 after:bg-[linear-gradient(to_right,white_0%,rgba(255,255,255,0)_100%)] after:content-['']">
            <div className="flex flex-col gap-7 py-4">
              <div className="block">
                <Imageslider />
              </div>

              <div className="hidden lg:block">
                <Imageslider />
              </div>

              <div className="hidden 2xl:block">
                <Imageslider />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
