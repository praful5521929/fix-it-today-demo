"use client";
import React from "react";
import Image from "next/image";
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card";
import Link from "next/link";
import { Button } from "@/components/ui/button";
import { ArrowRight } from "lucide-react";
import { Dialog, DialogContent, DialogDescription, DialogHeader, DialogTitle, DialogTrigger } from "@/components/ui/dialog";

export default function Home() {
  return (
    <div className="-mt-[70px]">
      <Dialog>
        <DialogContent className="px-6 py-5 pb-0 md:max-w-[500px] overflow-y-auto max-h-[85vh]">
          <DialogHeader>
            <DialogTitle className="text-left font-bold text-base text-black pb-3">Women&lsquo;s Salon, Spa & Laser Clinic</DialogTitle>

            <DialogDescription>
              <Card className="shadow-none border-0 mb-1">
                <CardHeader className="p-0 mb-4">
                  <CardTitle className="text-base text-black font-semibol text-left">Salon & Spa</CardTitle>
                </CardHeader>

                <CardContent className="p-0">
                  <div className="flex -mx-2 flex-wrap justify-start">
                    <div className="px-2 w-1/2 mb-4">
                      <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                        <CardHeader className="p-0 mb-0">
                          <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                            <Image alt="" src="/images/hair-color-sample.svg" width="32" height="32" className="object-content object-center"></Image>
                            Women&lsquo;s Salon, Spa & Laser Clinic
                          </CardTitle>
                        </CardHeader>
                      </Card>
                    </div>

                    <div className="px-2 w-1/2 mb-4">
                      <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                        <CardHeader className="p-0 mb-0">
                          <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                            <Image alt="" src="/images/hair-style.svg" width="32" height="32" className="object-content object-center"></Image>
                            Men&lsquo;s Salon & Massage
                          </CardTitle>
                        </CardHeader>
                      </Card>
                    </div>

                    <div className="px-2 w-1/2 mb-4">
                      <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                        <CardHeader className="p-0 mb-0">
                          <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                            <Image alt="" src="/images/air-conditioner.svg" width="32" height="32" className="object-content object-center"></Image>
                            AC & Appliance Repair
                          </CardTitle>
                        </CardHeader>
                      </Card>
                    </div>
                  </div>
                </CardContent>
              </Card>

              <Card className="shadow-none border-0 mb-1">
                <CardHeader className="p-0 mb-4">
                  <CardTitle className="text-base text-black font-semibol text-left">Laser Clinic</CardTitle>
                </CardHeader>

                <CardContent className="p-0">
                  <div className="flex -mx-2 flex-wrap justify-start">
                    <div className="px-2 w-1/2 mb-4">
                      <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                        <CardHeader className="p-0 mb-0">
                          <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                            <Image alt="" src="/images/hair-color-sample.svg" width="32" height="32" className="object-content object-center"></Image>
                            Women&lsquo;s Salon, Spa & Laser Clinic
                          </CardTitle>
                        </CardHeader>
                      </Card>
                    </div>

                    <div className="px-2 w-1/2 mb-4">
                      <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                        <CardHeader className="p-0 mb-0">
                          <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                            <Image alt="" src="/images/hair-style.svg" width="32" height="32" className="object-content object-center"></Image>
                            Men&lsquo;s Salon & Massage
                          </CardTitle>
                        </CardHeader>
                      </Card>
                    </div>
                  </div>
                </CardContent>
              </Card>
            </DialogDescription>
          </DialogHeader>
        </DialogContent>

        <main className="pb-0 pt-[70px] md:h-screen md:overflow-hidden">
          <div className="mx-auto container">
            <div className="flex -mx-2 flex-wrap relative">
              <div className="hidden md:block order-1 px-2 w-full md:w-[30%] xl:w-[43%] h-56 overflow-auto md:overflow-visible md:h-screen left-0 top-0 z-40 md:animate-loop-scroll">
                <div className="grid grid-cols-2 gap-4">
                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/rectone.png" width="170" height="324" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectthree.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/recttwo.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectfour.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/rectone.png" width="170" height="324" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectthree.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/recttwo.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectfour.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/rectone.png" width="170" height="324" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectthree.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/recttwo.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectfour.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/rectone.png" width="170" height="324" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectthree.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/recttwo.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectfour.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/rectone.png" width="170" height="324" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectthree.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/recttwo.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectfour.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/rectone.png" width="170" height="324" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectthree.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/recttwo.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectfour.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/rectone.png" width="170" height="324" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectthree.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/recttwo.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectfour.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/rectone.png" width="170" height="324" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectthree.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>

                  <div className="grid gap-4">
                    <div>
                      <Image alt="" src="/images/recttwo.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                    <div>
                      <Image alt="" src="/images/rectfour.png" width="170" height="245" className="object-cover object-center w-full"></Image>
                    </div>
                  </div>
                </div>
              </div>

              <div className="px-2 w-full md:w-[70%] xl:w-[57%] ml-auto mb-10 md:mb-0 order-2 md:pb-32 md:flex items-center">
                <div className="md:px-2 xl:px-8 md:-mt-12">
                  <Card className="shadow-none border-0 mt-4 2xl:mt-0 lg:mt-6 rounded-none">
                    <CardHeader className="p-0 mb-5">
                      <CardTitle className="mb-1 font-bold text-xl sm:text-2xl flex flex-wrap items-center justify-between gap-2">
                        What service are you looking for?
                        <Link href={"/"} className="text-sm text-blue-500 hover:text-blue-800 font-normal underline">
                          Explore More Services
                        </Link>
                      </CardTitle>
                    </CardHeader>

                    <CardContent className="p-0">
                      <div className="flex -mx-2 flex-wrap justify-start">
                        <div className="px-2 w-1/2 lg:w-1/3 ml-auto mb-4">
                          <DialogTrigger className="w-full">
                            <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                              <CardHeader className="p-0 mb-0">
                                <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                                  <Image alt="" src="/images/hair-color-sample.svg" width="32" height="32" className="object-content object-center"></Image>
                                  Women&lsquo;s Salon, Spa & Laser Clinic
                                </CardTitle>
                              </CardHeader>
                            </Card>
                          </DialogTrigger>
                        </div>

                        <div className="px-2 w-1/2 sm:w-1/3 ml-auto mb-4">
                          <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                            <CardHeader className="p-0 mb-0">
                              <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                                <Image alt="" src="/images/hair-style.svg" width="32" height="32" className="object-content object-center"></Image>
                                Men&lsquo;s Salon & Massage
                              </CardTitle>
                            </CardHeader>
                          </Card>
                        </div>

                        <div className="px-2 w-1/2 lg:w-1/3 ml-auto mb-4">
                          <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                            <CardHeader className="p-0 mb-0">
                              <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                                <Image alt="" src="/images/air-conditioner.svg" width="32" height="32" className="object-content object-center"></Image>
                                AC & Appliance Repair
                              </CardTitle>
                            </CardHeader>
                          </Card>
                        </div>

                        <div className="px-2 w-1/2 lg:w-1/3 ml-auto mb-4">
                          <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                            <CardHeader className="p-0 mb-0">
                              <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                                <Image alt="" src="/images/cleaning-materials.svg" width="32" height="32" className="object-content object-center"></Image>
                                Cleaning & Pest Control
                              </CardTitle>
                            </CardHeader>
                          </Card>
                        </div>

                        <div className="px-2 w-1/2 lg:w-1/3 ml-auto mb-4">
                          <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                            <CardHeader className="p-0 mb-0">
                              <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                                <Image alt="" src="/images/carpenter.svg" width="32" height="32" className="object-content object-center"></Image>
                                Electrician, Plumber & Carpenter
                              </CardTitle>
                            </CardHeader>
                          </Card>
                        </div>

                        <div className="px-2 w-1/2 lg:w-1/3 ml-auto mb-4">
                          <Card className="shadow-none border border-gray-300 overflow-hidden rounded-lg cursor-pointer p-2 sm:p-3 lg:p-4 bg-white hover:bg-blue-100 transition ease-in-out delay-200 h-full flex items-center justify-center sm:justify-start">
                            <CardHeader className="p-0 mb-0">
                              <CardTitle className="flex items-center gap-3 text-[12px] sm:text-sm text-black font-semibold flex-wrap justify-center flex-col sm:flex-nowrap sm:justify-start sm:flex-row text-center sm:text-left">
                                <Image alt="" src="/images/house-painting.svg" width="32" height="32" className="object-content object-center"></Image>
                                Painting & Waterproofing
                              </CardTitle>
                            </CardHeader>
                          </Card>
                        </div>
                      </div>
                    </CardContent>
                  </Card>

                  <div className="mx-auto max-w-xl w-full pt-2 text-center sm:text-left">
                    <Card className="shadow-none border-0 mt-2 sm:mt-5 md:mt-10 relative p-4 pb-0 sm:pb-4 rounded-lg  bg-gradient-to-r from-gray-100 to-gray-50 sm:pr-64">
                      <CardHeader className="p-0 mb-0">
                        <CardTitle className="mb-1 font-bold text-xl">Register as a Service Provider</CardTitle>
                      </CardHeader>
                      <CardContent className="p-0">
                        <p className="sm:line-clamp-2 mb-3 text-sm sm:text-[10px]">
                          Lorem ipsum dolor sit amet consectetur. Ante purus ultricies enim consequat neque faucibus elementum dolor nullam.
                        </p>
                        <Link href={"/"} className="text-blue-500 hover:text-blue-800 inline-flex items-center gap-2 font-semibold">
                          Register
                          <span className="-rotate-45">
                            <ArrowRight className="animate-bounce" size={"16"} />
                          </span>
                        </Link>
                      </CardContent>

                      <div className="sm:absolute right-0 bottom-0 pointer-events-none max-w-[250px] mx-auto sm:ml-0 pt-7 sm:pt-0">
                        <Image alt="" src="/images/team.png" width="250" height="167" className="object-content object-center max-w-full"></Image>
                      </div>
                    </Card>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </Dialog>
    </div>
  );
}
