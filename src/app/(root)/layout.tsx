import Header from "@/components/Header";
import Footer from "@/components/Footer";
import React from "react";

export default function Home({ children }: { children: React.ReactNode }) {
  return (
    <div className="flex flex-col min-h-[calc(100vh-57px)] md:min-h-screen">
      <Header />
      <div className="flex-grow">{children}</div>
      <Footer />
    </div>
  );
}
