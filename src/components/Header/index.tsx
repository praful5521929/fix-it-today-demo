"use client";
import React from "react";
import { Popover, PopoverContent, PopoverTrigger } from "../ui/popover";
import { Button } from "../ui/button";
import { ChevronDown, MapPin, Search, LocateFixed, HelpCircle, User, ClipboardList, LogIn } from "lucide-react";
import { Input } from "../ui/input";
import Image from "next/image";
import Link from "next/link";
import { useState, useEffect } from "react";

export default function Header() {
  const [open, setOpen] = React.useState(false);

  const [isScrolled, setIsScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.scrollY;
      setIsScrolled(scrollTop > 0);
    };

    // Add scroll event listener
    window.addEventListener("scroll", handleScroll);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <>
      <div className="shadow-sm py-3 border-b sticky top-0 bg-white z-50">
        <div className="mx-auto container">
          <div className="flex items-center mx-0 sm:-mx-2 flex-wrap">
            <div className="hidden md:block w-full md:w-1/6 lg:w-1/4 px-2">
              <Link href="/">
                <Image alt="FIX IT TODAY" src="../images/logo.svg" width="170" height="30"></Image>
              </Link>
            </div>

            <div className="w-full md:w-[66.66%] lg:w-3/6 px-0 sm:px-2">
              <div className="sm:border border-gray-200 rounded-lg py-0">
                <div className="flex items-center flex-wrap sm:flex-nowrap">
                  <div className={`sm:border-r-2 sm:border-gray-300 sm:px-3 w-auto text-sm sm:w-60 mb-2 sm:mb-0 ${isScrolled ? "hidetopbar" : ""}`}>
                    <div className="flex items-center gap-2 flex-nowrap">
                      <MapPin size={20} className="text-gray-400" />

                      <p className="sm:text-[11px] lg:text-sm h-5 p-0 focus-visible:ring-0 border-0 focus-visible:ring-transparent focus-visible:ring-offset-0 line-clamp-1">
                        Ahmedabad, Gujarat
                      </p>

                      <Popover open={open} onOpenChange={setOpen}>
                        <PopoverTrigger asChild>
                          <Button variant="outline" aria-expanded={open} aria-label="Select Location" className="hover:bg-transparent border-0 p-0 ml-auto h-5">
                            <ChevronDown className="text-gray-500" size={18} />
                          </Button>
                        </PopoverTrigger>
                        <PopoverContent className="w-[270px] sm:w-[360px] p-0 me-20 ml-2 mt-3 border border-gray-300 rounded-lg overflow-hidden">
                          <div className="p-4 border-b bg-white hover:bg-blue-50 cursor-pointer">
                            <div className="flex gap-2">
                              <LocateFixed className="text-red-600 relative top-1" size={20} />
                              <div className="flex flex-col gap-0">
                                <h2 className="text-base text-red-600">Detect current location</h2>
                                <p className="text-sm text-gray-500">Using GPS</p>
                              </div>
                            </div>
                          </div>

                          <div className="p-4">Recent Locations</div>
                        </PopoverContent>
                      </Popover>
                    </div>
                  </div>

                  <div className="sm:px-3 w-full sm:w-[initial] sm:flex-auto">
                    <Input
                      icon={<Search size="lg" className="mt-1 text-gray-400" />}
                      className="sm:text-[11px] lg:text-sm focus-visible:ring-0 sm:border-0 focus-visible:ring-transparent focus-visible:ring-offset-0"
                      placeholder="Search for store, coupon & Offers..."
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className="hidden md:block w-full md:w-1/6 lg:w-1/4 px-2">
              <div className="flex items-center justify-end">
                <Button variant="default" size="sm" aria-expanded={open} aria-label="Select a team" className="min-w-[80px]">
                  <LogIn size={16} className="mr-1" /> Login
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="md:hidden shadow-top py-2 px-2 border-t fixed bottom-0 left-0 right-0 bg-white z-[99]">
        <div className="flex flex-nowrap items-center gap-5 sm:gap-10 ml-auto">
          <div className="w-1/4 flex items-center justify-center text-center">
            <Link href="/">
              <Image alt="FIX IT TODAY" src="../images/logo-icon.svg" className="object-contain object-center" width="30" height="30"></Image>
            </Link>
          </div>

          <div className="w-1/4 flex items-center justify-center text-center">
            <Link className="text-black text-xs flex justify-center text-center items-center flex-col gap-1" href={"/"}>
              <ClipboardList size={20} className="text-gray-300" />
              Booking
            </Link>
          </div>

          <div className="w-1/4 flex items-center justify-center text-center">
            <Link className="text-black text-xs flex justify-center text-center items-center flex-col gap-1" href={"/"}>
              <HelpCircle size={20} className="text-gray-300" />
              Help
            </Link>
          </div>

          <div className="w-1/4 flex items-center justify-center text-center">
            <Link className="text-black text-xs flex justify-center text-center items-center flex-col gap-1" href={"/"}>
              <User size={20} className="text-gray-300" />
              Account
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
