"use client";
import React from "react";
import Link from "next/link";

export default function Footer() {
  const [open, setOpen] = React.useState(false);
  return (
    <div className="md:fixed bottom-0 left-0 right-0 z-50 bg-black md:bg-opacity-50 py-4">
      <div className="mx-auto container">
        <div className="flex items-center -mx-2 flex-wrap">
          <div className="w-full px-2">
            <ul className="flex items-center gap-x-5 gap-y-2 justify-center flex-wrap">
              <li>
                <Link className="text-white text-sm" href={"/"}>
                  Services
                </Link>
              </li>
              <li>
                <Link className="text-white text-sm" href={"/"}>
                  About
                </Link>
              </li>
              <li>
                <Link className="text-white text-sm" href={"/"}>
                  Privacy Policy
                </Link>
              </li>
              <li>
                <Link className="text-white text-sm" href={"/"}>
                  Terms of Use
                </Link>
              </li>
              <li>
                <Link className="text-white text-sm" href={"/"}>
                  Contact
                </Link>
              </li>
              <li>
                <Link className="text-white text-sm" href={"/"}>
                  Help
                </Link>
              </li>
              <li>
                <Link className="text-white text-sm" href={"/"}>
                  FAQs
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
