"use client";
import React from "react";
import Image from "next/image";

const LOGOS = [
  <Image key="logo1" alt="" src="/images/rectone.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo2" alt="" src="/images/rectfour.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo3" alt="" src="/images/rectone.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo4" alt="" src="/images/rectfour.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo5" alt="" src="/images/rectone.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo6" alt="" src="/images/rectfour.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo7" alt="" src="/images/rectone.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo8" alt="" src="/images/rectfour.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo9" alt="" src="/images/rectone.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo10" alt="" src="/images/rectfour.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo11" alt="" src="/images/rectone.png" width="220" height="324" className="object-cover object-center"></Image>,
  <Image key="logo12" alt="" src="/images/rectfour.png" width="220" height="324" className="object-cover object-center"></Image>,
];

export default function imageslider() {
  return (
    <div className="relative m-auto w-full overflow-hiddenbg-white">
      <div className="animate-infinite-slider flex gap-8">
        {LOGOS.map((logo, index) => (
          <div className="slide flex w-[220px] items-center justify-center flex-none" key={index}>
            {logo}
          </div>
        ))}
        {LOGOS.map((logo, index) => (
          <div className="slide flex w-[220px] items-center justify-center flex-none" key={index}>
            {logo}
          </div>
        ))}
      </div>
    </div>
  );
}
